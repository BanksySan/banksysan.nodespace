namespace BanksySan.NodeSpace
{
    using System.Collections.Generic;
    using System.Diagnostics;

    [DebuggerStepThrough]
    internal class NodeResponse<TData, TInvocation, TResponse> : INodeResponse<TData, TInvocation, TResponse>
    {
        public NodeResponse(INode<TData, TInvocation, TResponse> node,
                            IEnumerable<INodeResponse<TData, TInvocation, TResponse>> childResponses,
                            TInvocation argument, TResponse response)
        {
            Node = node;
            ChildResponses = childResponses;
            Argument = argument;
            Response = response;
        }

        public INode<TData, TInvocation, TResponse> Node { get; }
        public IEnumerable<INodeResponse<TData, TInvocation, TResponse>> ChildResponses { get; }
        public TInvocation Argument { get; }
        public TResponse Response { get; }
    }
}