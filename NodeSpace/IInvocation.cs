namespace BanksySan.NodeSpace
{
    public interface IInvocation<TData, TInvocation, TResponse>
    {
        INodeResponse<TData, TInvocation, TResponse> Invoke(TInvocation argument = default(TInvocation));
    }
}