namespace BanksySan.NodeSpace
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;

    public class Node<TData, TInvocation, TResponse> : INode<TData, TInvocation, TResponse>,
                                                       IEquatable<Node<TData, TInvocation, TResponse>>
    {
        private readonly IInvocationSettings<TData, TInvocation, TResponse> _invocationSettings;

        private readonly ISet<INode<TData, TInvocation, TResponse>> _links =
            new HashSet<INode<TData, TInvocation, TResponse>>();

        public Node(string name, TData data = default(TData),
                    IInvocationSettings<TData, TInvocation, TResponse> invocationSettings = null)
        {
            Data = data;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            _invocationSettings = invocationSettings ?? InvocationSettings<TData, TInvocation, TResponse>.Default;
        }

        public IEnumerable<INode<TData, TInvocation, TResponse>> Links => _links;

        public TData Data { get; }

        public string Name { get; }

        public INodeResponse<TData, TInvocation, TResponse> Invoke(TInvocation argument)
        {
            var mutatedArgument = _invocationSettings.PreMutator(this, argument);

            var results = new ConcurrentStack<INodeResponse<TData, TInvocation, TResponse>>();


            Parallel.ForEach(_links,
                             link =>
                             {
                                 var result = link.Invoke(mutatedArgument);
                                 results.Push(result);
                             });

            var thisResult = _invocationSettings.Invocation(this, results, mutatedArgument);

            return new NodeResponse<TData, TInvocation, TResponse>(this, results, argument, thisResult);
        }

        [DebuggerStepThrough]
        public bool AddLink(INode<TData, TInvocation, TResponse> target)
        {
            return _links.Add(target);
        }

        [DebuggerStepThrough]
        public bool RemoveLink(INode<TData, TInvocation, TResponse> link)
        {
            return _links.Remove(link);
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return Name;
        }

        #region Equality

        [DebuggerStepThrough]
        public bool Equals(Node<TData, TInvocation, TResponse> other)
        {
            return string.Equals(Name, other?.Name);
        }

        [DebuggerStepThrough]
        public bool Equals(INode<TData, TInvocation, TResponse> other)
        {
            if (ReferenceEquals(null, other)) { return false; }
            if (ReferenceEquals(this, other)) { return true; }

            return string.Equals(Name, other.Name, StringComparison.Ordinal);
        }

        [DebuggerStepThrough]
        public override bool Equals(object obj)
        {
            var other = obj as Node<TData, TInvocation, TResponse>;
            return other != null && Equals(other);
        }

        [DebuggerStepThrough]
        public static bool operator ==(Node<TData, TInvocation, TResponse> left,
                                       Node<TData, TInvocation, TResponse> right)
        {
            return Equals(left, right);
        }

        [DebuggerStepThrough]
        public static bool operator !=(Node<TData, TInvocation, TResponse> left,
                                       Node<TData, TInvocation, TResponse> right)
        {
            return !Equals(left, right);
        }

        [DebuggerStepThrough]
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        #endregion
    }
}