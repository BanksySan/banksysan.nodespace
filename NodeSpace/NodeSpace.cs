﻿namespace BanksySan.NodeSpace
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class NodeSpace<TData, TInvocation, TResponse> : INodeSpace<TData, TInvocation, TResponse>
    {
        private readonly IList<Node<TData, TInvocation, TResponse>> _nodes;

        [DebuggerStepThrough]
        public NodeSpace(IEnumerable<Node<TData, TInvocation, TResponse>> nodes)
        {
            _nodes = nodes?.ToList() ?? throw new ArgumentNullException(nameof(nodes));
        }

        [DebuggerStepThrough]
        public NodeSpace(Node<TData, TInvocation, TResponse>[] nodes) : this(nodes?.ToList())
        {
        }

        public IEnumerable<INode<TData, TInvocation, TResponse>> Roots
        {
            get { return _nodes.Where(node => !node.Links.Any()); }
        }

        [DebuggerStepThrough]
        public void AddNode(string id, TData data = default(TData),
                            IInvocationSettings<TData, TInvocation, TResponse> invocation = null)
        {
            var node = new Node<TData, TInvocation, TResponse>(id, data, invocation);
            _nodes.Add(node);
        }

        public IEnumerable<INode<TData, TInvocation, TResponse>> GetSources(INode<TData, TInvocation, TResponse> node)
        {
            return _nodes.Where(n => n.Links.Contains(node));
        }

        public IEnumerable<INode<TData, TInvocation, TResponse>> GetTargets(INode<TData, TInvocation, TResponse> node)
        {
            return _nodes.Single(n => Equals(n, node)).Links;
        }

        public IEnumerable<INode<TData, TInvocation, TResponse>> Nodes => _nodes;
    }
}