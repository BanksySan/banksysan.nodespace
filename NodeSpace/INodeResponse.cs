namespace BanksySan.NodeSpace
{
    using System.Collections.Generic;

    public interface INodeResponse<TData, TInvocation, TResponse>
    {
        INode<TData, TInvocation, TResponse> Node { get; }
        IEnumerable<INodeResponse<TData, TInvocation, TResponse>> ChildResponses { get; }
        TInvocation Argument { get; }
        TResponse Response { get; }
    }
}