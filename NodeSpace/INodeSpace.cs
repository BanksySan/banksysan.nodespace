﻿namespace BanksySan.NodeSpace
{
    using System.Collections.Generic;

    public interface
        INodeSpace<TData, TInvocationInitial, TInvocationResponse>
    {
        IEnumerable<INode<TData, TInvocationInitial, TInvocationResponse>> Nodes { get; }

        void AddNode(string id, TData data = default(TData),
                     IInvocationSettings<TData, TInvocationInitial, TInvocationResponse> invocation = null);

        IEnumerable<INode<TData, TInvocationInitial, TInvocationResponse>> GetSources(
            INode<TData, TInvocationInitial, TInvocationResponse> node);

        IEnumerable<INode<TData, TInvocationInitial, TInvocationResponse>> GetTargets(
            INode<TData, TInvocationInitial, TInvocationResponse> node);
    }
}