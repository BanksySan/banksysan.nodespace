namespace BanksySan.NodeSpace
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    [DebuggerStepThrough]
    public class InvocationSettings<TData, TInvocation, TResponse> : IInvocationSettings<TData, TInvocation, TResponse>
    {
        private static readonly
            Func<
                INode<TData, TInvocation, TResponse>,
                IEnumerable<INodeResponse<TData, TInvocation, TResponse>>,
                TInvocation,
                TResponse>
            NO_OP_INVOCATION = (node, responses, arg) => default(TResponse);

        private static readonly Func<INode<TData, TInvocation, TResponse>, TInvocation, TInvocation> NO_OP_PRE_MUTATOR =
            (node, arg) => arg;

        /// <inheritdoc />
        public InvocationSettings(
            Func<
                    INode<TData, TInvocation, TResponse>,
                    IEnumerable<INodeResponse<TData, TInvocation, TResponse>>,
                    TInvocation,
                    TResponse>
                action = null,
            Func<
                INode<TData, TInvocation, TResponse>,
                TInvocation, TInvocation> preMutator = null)
        {
            PreMutator = preMutator ?? NO_OP_PRE_MUTATOR;
            Invocation = action ?? NO_OP_INVOCATION;
        }

        public static IInvocationSettings<TData, TInvocation, TResponse> Default =>
            new InvocationSettings<TData, TInvocation, TResponse>();

        /// <inheritdoc />
        public Func<INode<TData, TInvocation, TResponse>, TInvocation, TInvocation> PreMutator { get; }

        /// <inheritdoc />
        public Func<INode<TData, TInvocation, TResponse>, IEnumerable<INodeResponse<TData, TInvocation, TResponse>>,
                TInvocation, TResponse>
            Invocation { get; }
    }
}