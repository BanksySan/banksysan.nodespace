﻿namespace BanksySan.NodeSpace
{
    using System;
    using System.Collections.Generic;

    public interface INode<TData, TInvocation, TResponse> : IEquatable<INode<TData, TInvocation, TResponse>>,
                                                            IInvocation<TData, TInvocation, TResponse>
    {
        string Name { get; }
        TData Data { get; }
        bool AddLink(INode<TData, TInvocation, TResponse> target);
        bool RemoveLink(INode<TData, TInvocation, TResponse> targetNode);
        IEnumerable<INode<TData, TInvocation, TResponse>> Links { get; }
    }
}