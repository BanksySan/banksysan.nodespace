namespace BanksySan.NodeSpace
{
    using System;
    using System.Collections.Generic;

    public interface IInvocationSettings<TData, TInitialVector, TResponse>
    {
        Func<INode<TData, TInitialVector, TResponse>, TInitialVector, TInitialVector> PreMutator { get; }

        Func<INode<TData, TInitialVector, TResponse>, IEnumerable<INodeResponse<TData, TInitialVector, TResponse>>,
                TInitialVector, TResponse>
            Invocation { get; }
    }
}