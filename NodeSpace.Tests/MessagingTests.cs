﻿namespace BanksySan.NodeSpace.Tests
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Xunit;

    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class MessagingTests
    {
        [Fact]
        public void ArgumentMutation()
        {
            const string INITIAL = "S ";

            string Invocation(INode<int, string, string> node,
                              IEnumerable<INodeResponse<int, string, string>> childResponses, string s)
            {
                return $"{s}{node.Name} I ";
            }

            string Mutation(INode<int, string, string> node, string s)
            {
                return $"{s}{node.Name} M ";
            }

            var node1 = new Node<int, string, string>("1",
                                                      1,
                                                      new InvocationSettings<int, string, string>(Invocation,
                                                                                                  Mutation));
            var node2 = new Node<int, string, string>("2",
                                                      2,
                                                      new InvocationSettings<int, string, string>(Invocation,
                                                                                                  Mutation));

            node1.AddLink(node2);
            var result = node1.Invoke(INITIAL);

            Assert.NotNull(result);
            Assert.Same(result.Node, node1);
            Assert.Equal(result.Response, "S 1 M 1 I ");
            Assert.Equal(result.Argument, INITIAL);
            Assert.Single(result.ChildResponses);
            var childNode = result.ChildResponses.SingleOrDefault();
            Assert.NotNull(childNode);
            Assert.Same(childNode.Node, node2);
            Assert.Equal(childNode.Argument, "S 1 M ");
            Assert.Empty(childNode.ChildResponses);
            Assert.Equal(childNode.Response, "S 1 M 2 M 2 I ");
        }

        [Fact]
        public void NodeInvocation()
        {
            const byte INITIAL = 0b1100;

            const byte B1 = 0b0001;
            const byte B2 = 0b0010;
            var node1 = new Node<int, byte, byte>("1",
                                                  invocationSettings: new
                                                      InvocationSettings<int, byte, byte>((node, childResults, b) =>
                                                                                              B1 ^ INITIAL));
            var node2 = new Node<int, byte, byte>("2",
                                                  invocationSettings: new
                                                      InvocationSettings<int, byte, byte>((node, childResults, b) =>
                                                                                              B2 ^ INITIAL));
            node1.AddLink(node2);
            var result = node1.Invoke(INITIAL);

            Assert.NotNull(result);
            Assert.Same(result.Node, node1);
            Assert.Equal(result.Response, INITIAL ^ B1);
            Assert.Equal(result.Argument, INITIAL);
            Assert.Single(result.ChildResponses);
            var childNode = result.ChildResponses.SingleOrDefault();
            Assert.NotNull(childNode);
            Assert.Same(childNode.Node, node2);
            Assert.Equal(childNode.Argument, INITIAL);
            Assert.Empty(childNode.ChildResponses);
            Assert.Equal(childNode.Response, INITIAL ^ B2);
        }
    }
}