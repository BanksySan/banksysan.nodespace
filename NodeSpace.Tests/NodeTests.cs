﻿#pragma warning disable CS1718 // Comparison made to same variable
namespace BanksySan.NodeSpace.Tests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Xunit;


    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "EqualExpressionComparison")]
    [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
    public class NodeTests
    {
        [Fact]
        public void DataCorrectlyStored()
        {
            const string ID = "1";
            var dateTime = new DateTime(2, 3, 4);

            var node = new Node<DateTime, int, int>(ID, dateTime);
            Assert.Equal(node.Name,ID);
            Assert.Equal(node.Data,dateTime);
        }

        [Fact]
        public void EqualityImplementation()
        {
            var node1 = new Node<object, int, int>("dummy-id");
            var node2 = new Node<object, int, int>("dummy-id");
            var nodeDifferent = new Node<object, int, int>("different");


            Assert.NotSame(node1, node2);
            Assert.Equal(node1, node2);
            Assert.Equal(node2, node1);
            Assert.NotEqual(node1, nodeDifferent);

            var node1EqualsNode1 = node1 == node1;
            var node1EqualsNode2 = node1 == node2;

            Assert.True(node1EqualsNode1);
            Assert.True(node1EqualsNode2);

            var node1NotEqualsNode1 = node1 != node1;
            var node1NotEqualsNode2 = node1 != node2;

            Assert.False(node1NotEqualsNode1);
            Assert.False(node1NotEqualsNode2);

            var node1EqualsNodeDifferent = node1 == nodeDifferent;
            var node1NotEqualsNodeDifferent = node1 != nodeDifferent;

            Assert.False(node1EqualsNodeDifferent);
            Assert.True(node1NotEqualsNodeDifferent);

            Assert.NotNull(node1);
            Assert.NotEqual(null, node1);

            Assert.False(null == node1);
            Assert.False(node1 == null);

            Assert.True(null != node1);
            Assert.True(node1 != null);
        }
    }
}