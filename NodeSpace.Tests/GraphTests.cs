﻿namespace BanksySan.NodeSpace.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Xunit;


    public class GraphTests
    {
        [Fact]
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public void ConsoleGraph()
        {
            var node1 = new Node<int, int, int>("1");
            var node2 = new Node<int, int, int>("2");
            var node3 = new Node<int, int, int>("3");
            var node4 = new Node<int, int, int>("4");
            var node5 = new Node<int, int, int>("5");
            var node6 = new Node<int, int, int>("6");

            node1.AddLink(node2);
            node1.AddLink(node3);
            node2.AddLink(node3);
            node2.AddLink(node4);
            node3.AddLink(node5);
            node2.AddLink(node6);

            var nodeSpace = new NodeSpace<int, int, int>(new[] {node6, node1, node2, node3, node4, node5});


            var visited = new HashSet<INode<int, int, int>>();
            var unvisited = new HashSet<INode<int, int, int>>(nodeSpace.Nodes);
            const string VISITED = "Visited";
            const string UNVISITED = "Unvisited";
            const string SKIP = "Skipped";
            const string PROCESS = "Process";

            Console.WriteLine($"{nameof(node1)} {node1.GetHashCode():X}");
            Console.WriteLine($"|-> {string.Join(", ", nodeSpace.GetTargets(node1).Select(x => x.GetHashCode().ToString("X")))}");
            Console.WriteLine($"{nameof(node2)} {node2.GetHashCode():X}");
            Console.WriteLine($"|-> {string.Join(", ", nodeSpace.GetTargets(node2).Select(x => x.GetHashCode().ToString("X")))}");
            Console.WriteLine($"{nameof(node3)} {node3.GetHashCode():X}");
            Console.WriteLine($"|-> {string.Join(", ", nodeSpace.GetTargets(node3).Select(x => x.GetHashCode().ToString("X")))}");
            Console.WriteLine($"{nameof(node4)} {node4.GetHashCode():X}");
            Console.WriteLine($"|-> {string.Join(", ", nodeSpace.GetTargets(node4).Select(x => x.GetHashCode().ToString("X")))}");
            Console.WriteLine($"{nameof(node5)} {node5.GetHashCode():X}");
            Console.WriteLine($"|-> {string.Join(", ", nodeSpace.GetTargets(node5).Select(x => x.GetHashCode().ToString("X")))}");
            Console.WriteLine($"{nameof(node6)} {node6.GetHashCode():X}");
            Console.WriteLine($"|-> {string.Join(", ", nodeSpace.GetTargets(node6).Select(x => x.GetHashCode().ToString("X")))}");

            while (unvisited.Any())
            {
                var newStack = unvisited.ToArray();
                Console.WriteLine();
                for (var i = 0; i < newStack.Length; i++)
                {
                    var node = newStack[i];

                    Console.Write($"{i,-5}|{node.GetHashCode(),10:X}|");
                    if (visited.Contains(node)) { Console.Write($"{VISITED,13}|{SKIP,8}|"); }
                    else
                    {
                        var dependencies = nodeSpace.GetTargets(node).ToArray();
                        Console.Write($"{UNVISITED,13}|");

                        if (dependencies.Intersect(visited).SequenceEqual(dependencies))
                        {
                            Console.Write($"{PROCESS,10}|");
                            unvisited.Remove(node);
                            visited.Add(node);
                        }
                        else { Console.Write($"{SKIP,10}|"); }
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}