﻿namespace BanksySan.NodeSpace.Tests
{
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Xunit;

    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class NodeSpaceTests
    {
        [Fact]
        public void AllNodesRetrieved()
        {
            var node1 = new Node<object, int, int>("1");
            var node2 = new Node<object, int, int>("2");

            var nodes = new[] {node1, node2};
            var nodeSpace = new NodeSpace<object, int, int>(nodes);

            Assert.Equal(nodeSpace.Nodes, nodes);
        }

        [Fact]
        public void FindMultipleRoots()
        {
            var node1 = new Node<int, int, int>("1");
            var node2 = new Node<int, int, int>("2");
            var node3 = new Node<int, int, int>("3");
            var node4 = new Node<int, int, int>("4");
            var node5 = new Node<int, int, int>("5");

            node1.AddLink(node2);
            node1.AddLink(node2);
            node3.AddLink(node4);
            node3.AddLink(node5);

            var nodeSpace = new NodeSpace<int, int, int>(new[] {node1, node2, node3, node4, node5});

            var roots = nodeSpace.Roots;

            Assert.Equal(roots, new[] {node2, node4, node5});
        }

        [Fact]
        public void FindRoots()
        {
            var node1 = new Node<int, int, int>("1");
            var node2 = new Node<int, int, int>("2");
            var node3 = new Node<int, int, int>("3");
            var node4 = new Node<int, int, int>("4");
            var node5 = new Node<int, int, int>("5");

            node1.AddLink(node2);
            node2.AddLink(node3);
            node3.AddLink(node4);
            node4.AddLink(node5);

            var nodeSpace = new NodeSpace<int, int, int>(new[] {node1, node2, node3, node4, node5});

            var roots = nodeSpace.Roots;

            Assert.Equal(roots,new[] {node5});
        }

        [Fact]
        public void SourcesRetrieved()
        {
            var node1 = new Node<object, int, int>("1");
            var node2 = new Node<object, int, int>("2");
            var node3 = new Node<object, int, int>("3");

            node1.AddLink(node2);
            node1.AddLink(node3);

            var nodeSpace = new NodeSpace<object, int, int>(new[] {node3, node1, node2});

            Assert.Empty(nodeSpace.GetSources(node1));
                Assert.Equal(nodeSpace.GetSources(node2), new[] {node1});
                Assert.Equal(nodeSpace.GetSources(node3), new[] {node1});
        }

        [Fact]
        public void TargetsRetrieved()
        {
            var node1 = new Node<object, int, int>("1");
            var node2 = new Node<object, int, int>("2");
            var node3 = new Node<object, int, int>("3");

            node1.AddLink(node2);
            node1.AddLink(node3);

            var nodeSpace = new NodeSpace<object, int, int>(new[] {node3, node1, node2});

                Assert.Equal(nodeSpace.GetTargets(node1),new[] {node2, node3});
                Assert.Equal(nodeSpace.GetTargets(node2), Enumerable.Empty<INode<object, int, int>>());
                Assert.Equal(nodeSpace.GetTargets(node3), Enumerable.Empty<INode<object, int, int>>());
        }
    }
}